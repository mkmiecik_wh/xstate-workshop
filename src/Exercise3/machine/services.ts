import { Receiver, Sender } from "xstate";
import ChatService, { ChatMessageEvent } from "../chat";

export const manageChatConnection =
  (context: { username: string }) =>
  (
    callback: Sender<
      | { type: "CONNECTED" }
      | { type: "MESSAGE_RECEIVED"; message: ChatMessageEvent }
    >,
    onReceive: Receiver<{ type: "MESSAGE_SENT_INFO"; message: string }>
  ) => {
    // instation
    const chat = ChatService();
    chat.onMessage((message: ChatMessageEvent) =>
      callback({
        type: "MESSAGE_RECEIVED",
        message,
      })
    );

    onReceive((e) => {
      if (e.type === "MESSAGE_SENT_INFO") {
        chat.sendMessage(e.message);
      }
    });

    chat.connect(context.username);
    callback({ type: "CONNECTED" });

    return () => {
      // cleanup function
      chat.disconnect();
      console.log("triggered on exit");
    };
  };
