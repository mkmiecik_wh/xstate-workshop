import './ex1.scss';
import TrafficLights from './TrafficLights';

import { useMachine } from '@xstate/react';
import lightsMachine from './machine';


const Exercise1 = () => {
  const [current, send] = useMachine(lightsMachine);

  return (
    <div className="ex1">
      <div>
        <button onClick={() => send('TICK')}>Tick</button>
      </div>
      <pre>{current.value}</pre>
      <div>
        <TrafficLights state={current.value as string} />
      </div>
    </div>
  );
}

export default Exercise1;