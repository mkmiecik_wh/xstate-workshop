module.exports = {
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": "ts-jest",
  },
  clearMocks: true,
  testEnvironment: "jsdom",
};
