import { createMachine } from 'xstate';

const lightsMachine = createMachine({
  initial: "red",
  states: {
    red: {
      on: {
        TICK: "red_amber",
      },
    },
    red_amber: {
      on: {
        TICK: "amber",
      },
    },
    green: {
      on: {
        TICK: "amber",
      },
    },
    amber: {
      on: {
        TICK: "red",
      },
    },
  },
});

export default lightsMachine;