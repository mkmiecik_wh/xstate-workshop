import cx from 'clsx';

const LIGHTS_STATES: {
  [key: string]: {
    red: boolean,
    yellow: boolean,
    green: boolean,
  }
} = {
  red: {
    red: true,
    yellow: false,
    green: false,
  },
  amber: {
    red: false,
    yellow: true,
    green: false,
  },
  green: {
    red: false,
    yellow: false,
    green: true,
  },
  red_amber: {
    red: true,
    yellow: true,
    green: false,
  },
};

const TrafficLights = ({ state }: { state: string }) => (
  <div className="traffic-light">
    <div className={cx("light red", { "on": LIGHTS_STATES[state].red })}></div>
    <div className={cx("light yellow", { "on": LIGHTS_STATES[state].yellow })}></div>
    <div className={cx("light green", { "on": LIGHTS_STATES[state].green })}></div>
  </div>
);

export default TrafficLights;
