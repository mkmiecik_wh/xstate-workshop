import { assign, createMachine, send } from "xstate";
import { ChatMessageEvent } from "../chat";

import { manageChatConnection } from "./services";

const chatMachine = createMachine<{
  username: string;
  messages: ChatMessageEvent[]
}>(
  {
    id: "root",
    initial: "disconnected",
    context: {
      username: "",
      messages: [],
    },
    states: {
      disconnected: {
        on: {
          CONNECT: {
            cond: "isUsernameFilled",
            target: "connection",
            actions: ["saveUsername"],
          },
        },
      },
      connection: {
        initial: "connecting",
        invoke: {
          id: "manager",
          src: "manageChatConnection",
        },
        states: {
          connecting: {
            on: {
              CONNECTED: "connected",
            },
          },
          connected: {
            type: "parallel",
            states: {
              connectionListener: {
                on: {
                  DISCONNECT: "#root.disconnected",
                },
              },
              messagesListener: {
                on: {
                  MESSAGE_RECEIVED: {
                    actions: "saveReceivedMessage",
                  },
                },
              },
              inputListener: {
                on: {
                  VIEW_MESSAGE_SENT: {
                    cond: "isMessagePresent",
                    actions: [
                      "logger",
                      send(
                        (context, event) => ({
                          type: "MESSAGE_SENT_INFO",
                          message: event.message,
                        }),
                        { to: "manager" }
                      ),
                    ],
                  },
                },
              },
            },
          },
        },
      },
    },
  },
  {
    guards: {
      isUsernameFilled: (context, event) => {
        return event.username && event.username.length > 0;
      },
      isMessagePresent: (context, event) =>
        event.message && event.message.length > 0,
    },
    actions: {
      logger: (context, event) => {
        console.log(event);
      },
      saveUsername: assign({
        username: (context, event) => event.username,
      }),
      saveReceivedMessage: assign({
        messages: (context, event) => {
          return [event.message, ...context.messages];
        }
      })
    },
    services: {
      manageChatConnection,
    },
  }
);

export default chatMachine;
