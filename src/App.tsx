import './App.scss'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink
} from "react-router-dom";
import Exercise1 from './Exercise1';
import Exercise2 from './Exercise2';
import Exercise3 from './Exercise3';
import Home from './Home';

function App() {
  return (
    <Router>
      <main className="App">
        <nav className="App-navigation">
          <ul>
            <li><NavLink exact activeClassName="active" to="/">Home</NavLink></li>
            <li><NavLink to="/ex-1">Exercise 1</NavLink></li>
            <li><NavLink to="/ex-2">Exercise 2</NavLink></li>
            <li><NavLink to="/ex-3">Exercise 3</NavLink></li>
          </ul>
        </nav>
        <section className="content">
          <Switch>
            <Route component={Home} exact path="/" />
            <Route component={Exercise1} path="/ex-1" />
            <Route component={Exercise2} path="/ex-2" />
            <Route component={Exercise3} path="/ex-3" />
          </Switch>
        </section>
      </main>
    </Router>
  )
}

export default App
