import './ex2.scss';

import { ErrorBox, FetchSuccessBox, FetchingBox, IdleBox } from './components/Boxes'
import { State } from 'xstate';
import { useMachine } from '@xstate/react';
import nameMachine from './machine';

const BoxRenderer = ({ state, sendEvent }: {
  state: State<{
    fetchedName: string,
    errorMessage: string,
  }>, sendEvent: (eventName: string) => void
}) => {
  if (state.value === 'idle') {
    return <IdleBox onClick={() => sendEvent('FETCH')} />
  }

  if (state.value === 'fetching') {
    return <FetchingBox />
  }

  if (state.value === 'loaded') {
    return <FetchSuccessBox name={state.context.fetchedName} onClick={() => sendEvent('FETCH')} />
  }

  if (state.value === 'errored') {
    return <ErrorBox message={state.context.errorMessage} onClick={() => sendEvent('FETCH')} />
  }

  return null;
}

const Exercise2 = () => {
  const [current, send] = useMachine(nameMachine);
  return (
    <div className="ex2">
      <pre>{current.value}</pre>
      <pre>{JSON.stringify(current.context)}</pre>
      <div>
        <BoxRenderer state={current} sendEvent={(event) => send(event)} />
      </div>
    </div>
  );
}

export default Exercise2