
import Loader from "./Loader";

export const IdleBox = ({ onClick }: { onClick: () => void }) => (
  <div className="box">
    <header>
      Why not to find some great name for your next initiative?
    </header>
    <p className="controls">
      <button onClick={onClick}>Find</button>
    </p>
  </div>
)

export const FetchingBox = () => (
  <div className="box">
    <p>Fetching...</p>
    <Loader />
  </div>
)

export const FetchSuccessBox = ({ name, onClick }: { name: string, onClick: () => void }) => (
  <div className="box">
    New name:
    <h3>
      {name}
    </h3 >
    <p className="controls">
      <button onClick={onClick}>Find new one</button>
    </p>
  </div>
)

export const ErrorBox = ({ message, onClick }: { message: string, onClick: () => void }) => (
  <div className="box">
    Ooops, there was a problem while fetching new name:
    <pre>{message}</pre>
    <p className="controls">
      <button onClick={onClick}>Find new one</button>
    </p>
  </div>
)
