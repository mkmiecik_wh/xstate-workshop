import './ex3.scss';

import { useState } from 'react';
import { useMachine } from '@xstate/react';
import chatMachine from './machine';
import { send } from 'xstate/lib/actionTypes';

const ConnectModal = ({ onConnect }: { onConnect: (username: string) => void }) => {
  const [inputValue, setInputValue] = useState('');
  return (
    <div className="connect-modal">
      <div>
        <p>Your name:</p>
        <p><input type="text" placeholder="username" value={inputValue} onChange={e => setInputValue(e.target.value)} /></p>
        <p>
          <button onClick={() => onConnect(inputValue)}>Connect</button></p>
      </div>
    </div>
  );
}

const MessageInput = ({ onSend }: { onSend: (message: string) => void }) => {
  const [inputValue, setInputValue] = useState('');
  return (
    <div className="text-input-container">
      <input type="text" value={inputValue} onChange={e => setInputValue(e.target.value)} />
      <button onClick={() => {
        onSend(inputValue);
        setInputValue('');
      }}>Send</button>
    </div>
  )
}

const Exercise3 = () => {
  const [current, send] = useMachine(chatMachine);
  return (
    <div className="ex3">
      {current.matches('disconnected') && <ConnectModal onConnect={(username) => send({ type: 'CONNECT', username })} />}

      {current.matches({
        "connection": "connecting"
      }) && (
          <div className="status-bar">
            <div>Connecting as <strong>{current.context.username}</strong></div>
          </div>
        )}

      {current.matches({
        "connection": "connected"
      }) && (<div className="status-bar">
        <div>Connected as <strong>{current.context.username}</strong></div>
        <button onClick={() => send({ type: 'DISCONNECT' })}>Disconnect</button>
      </div>)}
      

      <div className="messages-container">
        {current.context.messages.map((message, idx) => (
          <p key={idx}><strong>{message.time} {message.name}:</strong> {message.message}</p>
        ))}
      </div>

      <MessageInput onSend={(message) => send({ type: 'VIEW_MESSAGE_SENT', message })} />
    </div>
  );
}

export default Exercise3;
