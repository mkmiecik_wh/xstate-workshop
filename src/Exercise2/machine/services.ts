import faker from 'faker';

const wait = (time: number): Promise<void> =>
  new Promise((resolve) => setTimeout(() => resolve(), time));

export const fetchName = async () => {
  await wait(1000);
  return faker.company.companyName();
};


export const fetchNameRandomly = async () => {
  const random = Math.random();
  await wait(1000);
  if (random > 0.5) {
    throw new Error('oops. Request failed!');
  }
  return faker.company.companyName();
};
