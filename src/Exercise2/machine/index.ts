import { createMachine, assign } from "xstate";
import { fetchName, fetchNameRandomly } from "./services";

const nameMachine = createMachine<{
  fetchedName: string,
  errorMessage: string,
}>({
  initial: "idle",
  context: {
    fetchedName: '',
    errorMessage: '',
  },
  states: {
    idle: {
      on: {
        FETCH: {
          target: "fetching",
          actions: 'logger'
        }
      },
    },
    fetching: {
      entry: ['clearContext'],
      invoke: {
        src: 'fetchName',
        onDone: {
          actions: ['logger', 'saveFetchedName'],
          target: 'loaded'
        },
        onError: {
          actions: ['logger', 'saveErrorMessage'],
          target: 'errored'
        }
      },
      exit: [],
    },
    loaded: {
      on: {
        FETCH: "fetching",
      },
    },
    errored: {
      on: {
        FETCH: "fetching",
      },
    },
  },
}, {
  services: {
    fetchName: fetchNameRandomly
  },
  actions: {
    logger: (context, event) => {
      console.log(event);
    },
    clearContext: assign({
      fetchedName: '',
      errorMessage: ''
    }),
    saveFetchedName: assign({
      fetchedName: (context, event) => {
        return event.data;
      }
    }),
    saveErrorMessage: assign({
      errorMessage: (context, event) => event.data.message,
    })
  }
});

export default nameMachine;
