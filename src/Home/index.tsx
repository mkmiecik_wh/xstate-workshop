const Home = () => (
  <div className="content">
    <h1>XState - React Academy - 18 October 2021</h1>
    <p>Useful links:</p>
    <ul>
      <li><a href="https://xstate.js.org/viz/" target="_blank">https://xstate.js.org/viz/</a> &mdash; old visualizer </li>
      <li><a href="https://stately.ai/viz" target="_blank">https://stately.ai/viz</a> &mdash; new visualizer</li>
    </ul>
  </div>
);

export default Home;
