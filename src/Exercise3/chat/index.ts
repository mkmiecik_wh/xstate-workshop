import faker from "faker";

export type ChatMessageEvent = {
  type: "messageReceived";
  time: string;
  name: string;
  message: string;
};

const names = ["John", "Anna", "Paul", "Stefanie"];

const ChatService = () => {
  let connected = false;
  let username = "";
  const subscribers: ((event: ChatMessageEvent) => void)[] = [];

  function onMessage(callback: (event: ChatMessageEvent) => void) {
    subscribers.push(callback);
  }

  let messageInterval: number | null = null;
  function connect(connectionName: string) {
    connected = true;
    username = connectionName;
    console.log('Connected!');
    messageInterval = setInterval(() => {
      const rand = Math.random();
      if (rand < 0.5) {
        return;
      }
      const name = names[Math.floor(rand * names.length)];
      const now = new Date();
      subscribers.forEach((cb) =>
        cb({
          name,
          type: "messageReceived",
          time: `${now.getHours().toString().padStart(2, '0')}:${now.getMinutes().toString().padStart(2, '0')}`,
          message: faker.hacker.phrase(),
        })
      );
    }, 5000);
  }

  function disconnect() {
    if (!connected || !messageInterval) {
      return;
    }
    clearInterval(messageInterval);
  }

  function sendMessage(message: string) {
    if (!connected) {
      return;
    }

    const now = new Date();
    subscribers.forEach((cb) =>
      cb({
        message,
        name: username,
        time: `${now.getHours()}:${now.getMinutes()}`,
        type: "messageReceived",
      })
    );
  }

  return {
    onMessage,
    connect,
    sendMessage,
    disconnect,
  };
};

export default ChatService;
