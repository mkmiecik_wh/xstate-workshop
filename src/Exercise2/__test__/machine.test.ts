import { interpret } from "xstate";
import nameMachine from "../machine";

describe("name machine", () => {
  it("starts fetching after receiving FETCH event", () => {
    // start in "idle" state, trigger "FETCH" event
    const actualState = nameMachine.transition("idle", { type: "FETCH" });
    expect(actualState.matches("fetching")).toBeTruthy();
  });

  it('fetches name correctly and results in loaded state', (done) => {
    const mockMachine = nameMachine.withConfig({
      services: {
        fetchName: () => Promise.resolve("Expected Name")
      }
    });

    const machineService = interpret(mockMachine).onTransition(state => {
      if (state.matches('loaded')) {
        expect(state.context.fetchedName).toBe('Expected Name');
        done();
      }
    });

    machineService.start();
    machineService.send({ type: 'FETCH' })
  });

  it ('if fetch failes, state should be errored', (done) => {
    const mockMachine = nameMachine.withConfig({
      services: {
        fetchName: () => Promise.reject({ message: "Expected Error"})
      }
    });

    const machineService = interpret(mockMachine).onTransition(state => {
      if (state.matches('errored')) {
        expect(state.context.errorMessage).toBe('Expected Error');
        done();
      }
    });

    machineService.start();
    machineService.send({ type: 'FETCH' })
  })
});
